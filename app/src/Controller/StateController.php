<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\States\MappingStates;
use App\Entity\State;

class StateController extends AbstractController
{
    /**
     * @Route("/state", methods={"POST"})
     */
    public function index(Request $request)
    {
        $response = new JsonResponse();
        
        $em = $this->getDoctrine()->getManager();
        
        $stateName = $request->get('name');
        $stateZip = $request->get('zip');
        $stateCreator = $this->getUser()->getId();

        $state = new State();
        $state->setName($stateName);
        $state->setZip($stateZip);
        $state->setCreatorId($stateCreator);
        
        $em->persist($state);
        $em->flush();
        return $response->setStatusCode(201)->setData(array('message' => ('State '.$state->getName().' successfully created by '.$this->getUser()->getUsername())));
    }

    /**
     * @Route("state/", methods={"GET"})
     */
    public function getStates()
    {
        $statesArray = [];
        $response = new JsonResponse();
        $mappingService = new MappingStates();
        $states = $this->getDoctrine()->getRepository('App:State')->findAll();
        foreach ($states as $state) {
            $statesArray[] = $mappingService->mapStates($state);
        }
    
        $response->setData(array('states' => $statesArray));
        return $response;
    }

    /**
     * @Route("state/{id}", methods={"GET"})
     */
    public function getStatesByID($id)
    {
        $response = new JsonResponse();
        $mappingService = new MappingStates();
        $service = $this->getDoctrine()->getRepository('App:State')->find($id);
        if ($service) {
            $response->setData(array('state' => $mappingService->mapStates($service)));
            return $response;
        }
        
        return $response->setStatusCode(404)->setData(array('message' => 'State not found'));
    }

    /**
     * @Route("state/{id}", methods={"PUT"})
     */
    public function updateStatesByID(Request $request, $id)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $state = $em->getRepository('App:State')->findOneById($id);
        if ($state) {
            $stateName = $request->get('name');
            $stateZip = $request->get('zip');

            $state->setName($stateName);
            $state->setZip($stateZip);
            
            $em->persist($state);
            $em->flush();
            
            $response->setData(array('message' => sprintf('State %s successfully updated', $state->getName())));
            return $response;
        }
        
        return $response->setStatusCode(404)->setData(array('message' => 'State not found'));
    }

    /**
     * @Route("state/{id}", methods={"DELETE"})
     */
    public function deleteStatesByID(Request $request, $id)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('App:State')->findOneById($id);
        if ($service) {
            $em->remove($service); 
            $em->flush();
            $response->setData(array('message' => sprintf('State %s successfully deleted', $service->getName())));
            return $response;
        }
        
        return $response->setStatusCode(404)->setData(array('message' => 'State not found'));
    }
}
