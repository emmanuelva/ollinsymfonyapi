<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\Services\MappingServices;
use App\Entity\Service;

class ServiceController extends AbstractController
{
    /**
     * @Route("/service", methods={"POST"})
     */
    public function index(Request $request)
    {
        $response = new JsonResponse();
        
        $em = $this->getDoctrine()->getManager();
        
        $serviceName = $request->get('name');
        $serviceDescription = $request->get('description');
        $servicePrice = $request->get('price');
        $serviceCreator = $this->getUser()->getId();

        $service = new Service();
        $service->setName($serviceName);
        $service->setDescription($serviceDescription);
        $service->setPrice($servicePrice);
        $service->setCreatorId($serviceCreator);
        
        $em->persist($service);
        $em->flush();
        return $response->setStatusCode(201)->setData(array('message' => ('Service '.$service->getName().' successfully created by '.$this->getUser()->getUsername())));
    }

    /**
     * @Route("service/", methods={"GET"})
     */
    public function getServices()
    {
        $servicesArray = [];
        $response = new JsonResponse();
        $mappingService = new MappingServices();
        $services = $this->getDoctrine()->getRepository('App:Service')->findAll();
        foreach ($services as $service) {
            $servicesArray[] = $mappingService->mapServices($service);
        }
    
        $response->setData(array('services' => $servicesArray));
        return $response;
    }

    /**
     * @Route("service/{id}", methods={"GET"})
     */
    public function getServicesByID($id)
    {
        $response = new JsonResponse();
        $mappingService = new MappingServices();
        $service = $this->getDoctrine()->getRepository('App:Service')->find($id);
        if ($service) {
            $response->setData(array('service' => $mappingService->mapServices($service)));
            return $response;
        }
        
        return $response->setStatusCode(404)->setData(array('message' => 'Service not found'));
    }

    /**
     * @Route("service/{id}", methods={"PUT"})
     */
    public function updateServicesByID(Request $request, $id)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('App:Service')->findOneById($id);
        if ($service) {
            $serviceName = $request->get('name');
            $serviceDescription = $request->get('description');
            $servicePrice = $request->get('price');

            $service->setName($serviceName);
            $service->setDescription($serviceDescription);
            $service->setPrice($servicePrice);
            
            $em->persist($service);
            $em->flush();
            
            $response->setData(array('message' => sprintf('Service %s successfully updated', $service->getName())));
            return $response;
        }
        
        return $response->setStatusCode(404)->setData(array('message' => 'Service not found'));
    }

    /**
     * @Route("service/{id}", methods={"DELETE"})
     */
    public function deleteServicesByID(Request $request, $id)
    {
        $response = new JsonResponse();
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository('App:Service')->findOneById($id);
        if ($service) {
            $em->remove($service); 
            $em->flush();
            $response->setData(array('message' => sprintf('Service %s successfully deleted', $service->getName())));
            return $response;
        }
        
        return $response->setStatusCode(404)->setData(array('message' => 'Service not found'));
    }
}

?>