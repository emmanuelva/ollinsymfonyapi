<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Payum\Core\Model\Payment;
use Payum\Core\Model\CreditCard;
use Payum\Core\Request\GetHumanStatus;

class PaymentController extends Controller
{
    /**
     * @Route("/payment",  methods={"GET"})
     */
    public function index()
    {
        $paymentClass = Payment::class;
        $gatewayName = 'stripe_checkout';
        $storage = $this->get('payum')->getStorage('App\Entity\Payment');
        
        $card = new CreditCard();
        $card->setNumber('4242424242424242');
        $card->setExpireAt(new \DateTime('2018-10-10'));
        $card->setSecurityCode(123);

        $payment = $storage->create();
        $payment->setNumber(uniqid());
        $payment->setCurrencyCode('MXN');
        $payment->setTotalAmount(1000); // 10.00 MXN
        $payment->setCreditCard($card);

        $payment->setDescription('Payment Service');
        $payment->setClientId(uniqid());
        $payment->setClientEmail('foo@example.com');
        
        $storage->update($payment);
        
        $captureToken = $this->get('payum')->getTokenFactory()->createCaptureToken(
            $gatewayName, 
            $payment, 
            'app_payment_done'
        );
        
        return $this->redirect($captureToken->getTargetUrl()); 
    }

    /**
     * @Route("/done",  methods={"GET"})
     */
    public function doneAction(Request $request)
    {
        $token = $this->get('payum')->getHttpRequestVerifier()->verify($request);
        $gateway = $this->get('payum')->getGateway($token->getGatewayName());
        $gateway->execute($status = new GetHumanStatus($token));
        $payment = $status->getFirstModel();
        
        return new JsonResponse(array(
            'status' => $status->getValue(),
            'payment' => array(
                'total_amount' => $payment->getTotalAmount(),
                'currency_code' => $payment->getCurrencyCode(),
                'details' => $payment->getDetails(),
            ),
        ));
    }
}
