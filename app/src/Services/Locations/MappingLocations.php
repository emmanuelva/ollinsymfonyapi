<?php
namespace App\Services\Locations;

use App\Entity\Location;

class MappingLocations
{
    // Method to map a location as an object
    public function mapLocations(Location $location)
    {
        return [
            'id' => (int) $location->getId(),
            'name' => (string) $location->getName(),
            'zip' => (string) $location->getZip(),
            'stateId' => (int) $location->getStateid(),
            'creationDate' => (string) $location->getCreationdate(),
            'creator' => (int) $location->getCreatorid()
        ];
    }
}
