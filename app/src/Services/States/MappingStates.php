<?php
namespace App\Services\States;

use App\Entity\State;

class MappingStates
{
    // Method to map a state as an object
    public function mapStates(State $state)
    {
        return [
            'id' => (int) $state->getId(),
            'name' => (string) $state->getName(),
            'zip' => (string) $state->getZip(),
            'creator' => (int) $state->getCreatorid()
        ];
    }
}
