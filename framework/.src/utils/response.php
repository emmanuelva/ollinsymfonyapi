<?php

  /**
   * Method to send a success response
   * @param status_code success status code to set in the response
   * @param data data to send in the response
   * @param message message to send in the response
   */
  function successResponse($status_code = 200, $data = [], $message = NULL) {
    $informational_responses = ($status_code >= 100) && ($status_code <= 103);
    $success_responses = (($status_code >= 200) && ($status_code <= 208)) || $status_code == 226;
    $redirection_responses = ($status_code >= 300) && ($status_code <= 308);
    header('Content-Type: application/json');
    if ($informational_responses || $success_responses || $redirection_responses) {
      http_response_code($status_code);
    } else {
      http_response_code(200);
    }

    $responseData = array('data' => $data);
    if ($message) {
      $responseData['message'] = $message;
    }
    echo json_encode( $responseData );
  }

  /**
   * Method to send an error response
   * @param status_code success status code to set in the response
   * @param data data to send in the response
   * @param message message to send in the response
   */
  function errorResponse($status_code = 400, $message = NULL) {
    $client_errors = ($status_code >= 400) && ($status_code <= 451);
    $server_errors = ($status_code >= 500) && ($status_code <= 511);
    header('Content-Type: application/json');
    if ($client_errors || $server_errors) {
      http_response_code($status_code);
    } else {
      http_response_code(400);
    }
    $responseData = array('message' => $message ? $message : 'There was an error');
    echo json_encode( $responseData );
  }

?>